"""Double shear layer problem with the EDAC scheme"""

import numpy
import nice.mesh.api as mesh
from nice.solver.solver import EDACSolver2D
from nice.solver.boundary_conditions import CGVelocityPeriodic, PeriodicPressure2D

# domain and physical constants
dim = 2
xmin, ymin, zmin = 0,  0,  0
xmax, ymax, zmax = 1.0,  1.0, 1.0

dx = dy = dz = 1.0/256

# EDAC type discretization on a collocated grid
dtype = EDACSolver2D.dtype()

# the domain limits and grid
domain = mesh.Domain(dim, xmin, ymin, zmin, xmax, ymax, zmax)
grid = mesh.NpyGrid( domain, dx, dy, dz, dtype )

# construct the solver
solver = EDACSolver2D(
    dim=dim, grid=grid, Re=10000.0, pfreq=1000, tf=1.0, cfl=0.3)

solver.set_mach_number(0.1)

# set the boundary conditions 
solver.set_bc_north( CGVelocityPeriodic(grid) )
solver.set_bc_south( CGVelocityPeriodic(grid) )
solver.set_bc_east(  CGVelocityPeriodic(grid) )
solver.set_bc_west(  CGVelocityPeriodic(grid) )

# set boundary conditions for pressure
solver.set_pressure_bc_north( PeriodicPressure2D(grid) )
solver.set_pressure_bc_south( PeriodicPressure2D(grid) )
solver.set_pressure_bc_east(  PeriodicPressure2D(grid) )
solver.set_pressure_bc_west(  PeriodicPressure2D(grid) )

# initialize the problem
deltaw = 80.0
deltap = 0.05

cents = grid.get_field('centroid')[0]
xc = cents[:, :, 0]; yc = cents[:, :, 1]
u = grid.get_field('u0')[0]
v = grid.get_field('v0')[0]
p = grid.get_field('p0')[0]

i1 = numpy.where( yc <= 0.5 )[0]
i2 = numpy.where( yc >  0.5 )[0]

for j in range( 1, grid.jmax+1 ):
    for i in range( 1, grid.jmax+1 ):
        if yc[j, i] <= 0.5:
            u[j, i] = numpy.tanh( deltaw*(yc[j, i] - 0.25) )
        else:
            u[j, i] = numpy.tanh( deltaw*(0.75 - yc[j, i]) )
            
        v[j, i] = deltap * numpy.sin( 2*numpy.pi * (xc[j, i] + 0.25) )

# solve
solver.setup()
solver.solve()

"""Example script for the Lid Driven Cavity problem using the EDAC method"""

import nice.mesh.api as mesh
from nice.solver.solver import EDACSolver2D
from nice.solver.boundary_conditions import CGVelocityDirichlet, HomogenousNeumannPressure2D

from nice.ibm.api import Geometry2D, ImmersedBoundaryManager

# domain and physical constants
dim = 2
dx = dy = dz = 1./128
xmin, ymin, zmin = 0,  0,  0
xmax, ymax, zmax = 1.0,  1.0,  1.0

# We will use a standard Staggered Grid discretization
dtype = EDACSolver2D.dtype()

# the domain limits and grid
domain = mesh.Domain(dim, xmin, ymin, zmin, xmax, ymax, zmax)
grid = mesh.NpyGrid( domain, dx, dy, dz, dtype )

# construct the solver
solver = EDACSolver2D(
    dim=dim, grid=grid, Re=1000.0, pfreq=10000, steady_state=True,
    cfl=0.3)

solver.set_mach_number(0.1)
solver.set_steady_state_tolerance(1e-8)

# create the immersed boundary for the solver
Geometry2D.write_box(xc=0.5, yc=0.5, a=0.25, b=0.25)
geom = Geometry2D('box.dat')
ibm_manager = ImmersedBoundaryManager(grid, geom)
#solver.set_immersed_boundary_manager(ibm_manager, is_moving=False)

# set the boundary conditions 
solver.set_bc_north( CGVelocityDirichlet(grid, u=1.0, v=0.0) )
solver.set_bc_south( CGVelocityDirichlet(grid, u=0.0, v=0.0) )
solver.set_bc_west(  CGVelocityDirichlet(grid, u=0.0, v=0.0) )
solver.set_bc_east(  CGVelocityDirichlet(grid, u=0.0, v=0.0) )

# set boundary conditions for pressure
solver.set_pressure_bc_north( HomogenousNeumannPressure2D(grid) )
solver.set_pressure_bc_south( HomogenousNeumannPressure2D(grid) )
solver.set_pressure_bc_east(  HomogenousNeumannPressure2D(grid) )
solver.set_pressure_bc_west(  HomogenousNeumannPressure2D(grid) )

# initialize the problem
u = grid.get_field('u0')[0]; u[:] = 0.0
v = grid.get_field('v0')[0]; v[:] = 0.0
p = grid.get_field('p0')[0]; p[:] = 0.0

# solve
solver.setup()
solver.solve()

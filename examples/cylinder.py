"""Example script for flow past a sphere"""

import nice.mesh.api as mesh
from nice.solver.solver import SGPressureCorrectionSolver2D
from nice.solver.boundary_conditions import SGVelocityDirichlet2D, SGVelocityHomogenousNeumann2D,\
    HomogenousNeumannPressure2D, DirichletPressure2D

from nice.ibm.api import Geometry2D, ImmersedBoundaryManager

# domain and physical constants
dim=2
xmin, ymin, zmin = -5.0, -2.5, 0
xmax, ymax, zmax = 25.0, 2.5, 1
dx = dz = 0.1
dy = 0.05
Re = 100.0
uinflow = 1.0

# We will use a standard Staggered Grid discretization
dtype = SGPressureCorrectionSolver2D.dtype()

# the domain limits and grid
domain = mesh.Domain(dim, xmin, ymin, zmin, xmax, ymax, zmax)
grid = mesh.NpyGrid( domain, dx, dy, dz, dtype, is_staggered=True )

# construct the solver
solver = SGPressureCorrectionSolver2D(
    dim=dim, grid=grid, Re=Re, pfreq=100, steady_state=False, tf=10.0, cfl=0.3)

solver.set_donor_cell_discretization_gamma(gamma=0.5)
solver.set_ppe_max_iter(500000)
solver.set_outlet_pressure_dirichlet(True)

# create the immersed boundary manager for the solver
Geometry2D.write_cylinder(rad=1.0, npanels=120)
geom = Geometry2D('cylinder.dat')
ibm_manager = ImmersedBoundaryManager(grid, geom)

solver.set_immersed_boundary_manager(ibm_manager, is_moving=False)

# set the boundary conditions 
solver.set_bc_north( SGVelocityHomogenousNeumann2D(grid) )
solver.set_bc_south( SGVelocityHomogenousNeumann2D(grid) )
solver.set_bc_east(  SGVelocityHomogenousNeumann2D(grid) )

# inlet boundary conditions (Dirichlet)
solver.set_bc_west(  SGVelocityDirichlet2D(grid, u=uinflow, v=0.0) )

# initialize the problem
u = grid.get_field('u0')[0]; u[:] = uinflow
v = grid.get_field('v0')[0]; v[:] = 0.0
p = grid.get_field('p')[0]; p[:] = 0.0

# solve
solver.setup()
solver.solve()

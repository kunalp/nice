"""Defenitions for the boundary conditions"""
from nice.mesh.grid cimport NpyGrid

cdef class BoundaryCondition:
    cdef str face                        # face (W, E, N, S)
    cdef NpyGrid grid                    # underlying grid with solution values

    cdef public u, v                     # values for u & v along the face

    cpdef apply_bc(self, int stage=*)

# Staggered-grid velocity boundary conditions
cdef class SGVelocityDirichlet2D(BoundaryCondition):
    pass

cdef class SGVelocityHomogenousNeumann2D(BoundaryCondition):
    pass

cdef class SGVelocityOutflow2D(BoundaryCondition):
    pass

# Collocated-grid velocity boundary conditions
cdef class CGVelocityDirichlet(BoundaryCondition):
    pass

cdef class CGVelocityPeriodic(BoundaryCondition):
    pass

cdef class CGVelocityHomogeneousNeumann(BoundaryCondition):
    pass

cdef class HomogenousNeumannPressure2D(BoundaryCondition):
    pass

cdef class DirichletPressure2D(BoundaryCondition):
    cdef public double pressure

cdef class DKCDirichletPressure2D(BoundaryCondition):
    cdef public double Re
    cdef public double u0
    cdef public double delta

cdef class PeriodicPressure2D(BoundaryCondition):
    pass
